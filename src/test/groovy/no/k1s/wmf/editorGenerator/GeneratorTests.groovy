package no.k1s.wmf.editorGenerator
import spock.lang.*

class GeneratorTests extends spock.lang.Specification {
    
    @IgnoreIf({System.getenv("CI").equals("true")})
    def "generate simple AB editor"()
    {
        setup:
            println(System.getenv("CI"))
            def outputdir = new File("/tmp/abEditor")
            if(outputdir.exists() )
                outputdir.delete()
            outputdir.mkdir()
            println outputdir
            def generator = new Generator()
            println new File(".").absolutePath
        when:
            generator.generateElmCode(new File("./src/test/resources/My.ecore").text, "/tmp/abEditor", "ABPackage")
        then: 
            new File("/tmp/abEditor/src/Main.elm").exists()
    }
} 