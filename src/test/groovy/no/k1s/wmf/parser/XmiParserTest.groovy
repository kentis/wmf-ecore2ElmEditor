package no.k1s.wmf.parser

class XmiParserTest extends spock.lang.Specification {

    def "xmi file parses to not null"() {
        setup:
            def parser = new XmiParser()
        when:
            def res = parser.parse(new File("./src/test/resources/My.ecore"))
            println res
        then:
            res != null
    }

    def "xmi string parses to not null"() {
        setup:
            def parser = new XmiParser()
        when:
            def res = parser.parse(new File("./src/test/resources/My.ecore").text)
            println res
        then:
            res != null
    }

    
    def  "parsed xmi contains references"(){
        setup:
            def parser = new XmiParser()
        when:
            def pack = parser.parse(new File("./src/test/resources/My.ecore").text)
            def a  = pack.getEClassifiers().get(0)
            
            def ref = a.getEReferences()
            println ref.size()
            println ref
        then:
            a != null
            ref != null
            ref.size() == 1
            ref.get(0).getName().startsWith("arc")
    }


}
