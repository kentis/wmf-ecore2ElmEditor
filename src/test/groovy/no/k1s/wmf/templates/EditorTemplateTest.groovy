package no.k1s.wmf.templates

import org.k1s.templateTester.*
import no.k1s.wmf.parser.XmiParser
import java.lang.annotation.*

class EditorTemplateTest extends TemplateSpecification {

    def "generated code contains exposed function"() {
        setup:
            def parser = new XmiParser()
            def ecModel = parser.parse(new File("./src/test/resources/My.ecore"))
            
            def text = genTemplate("src/main/resources/templates/editor.tmpl.elm", [ecModel: ecModel, modelElement:"ABPackage"])
            println text
        expect:
            text.contains("module Editor exposing (renderEditor)")
            text.contains("renderEditor model =")
    }

}