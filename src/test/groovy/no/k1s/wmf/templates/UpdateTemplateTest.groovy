package no.k1s.wmf.templates

import org.k1s.templateTester.*
import java.lang.annotation.*
import no.k1s.wmf.parser.XmiParser


class UpdateTemplateTest extends TemplateSpecification {

    def "generated code contains exposed function"() {
        setup:
            def parser = new XmiParser()
            def ecModel = parser.parse(new File("./src/test/resources/My.ecore"))
            
            def text = genTemplate("src/main/resources/templates/update.tmpl.gsp", [ecModel: ecModel, modelElement:"ABPackage"])
            println text
        expect:
            text.contains("module Update exposing (update")
            text.contains("update msg model =")

    }

}