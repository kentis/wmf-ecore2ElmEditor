package no.k1s.wmf.templates


import org.k1s.templateTester.*
import java.lang.annotation.*
import no.k1s.wmf.parser.XmiParser

class TypesTemplateTest extends TemplateSpecification {
    
    def "Simple syntax"() {
        setup:
            def parser = new XmiParser()
            def ecModel = parser.parse(new File("./src/test/resources/My.ecore"))

            def text = genTemplate("src/main/resources/templates/types.tmpl.gsp", [ecModel: ecModel, modelElement:"ABPackage"])
            println text
        expect:
            text.contains("type alias Node_a =")
            text.contains("type alias Node_b =")
    }
}