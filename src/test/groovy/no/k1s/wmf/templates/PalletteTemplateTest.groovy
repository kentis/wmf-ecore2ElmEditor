package no.k1s.wmf.templates

import org.k1s.templateTester.*
import java.lang.annotation.*

class PalletteTemplateTest extends TemplateSpecification {

    def "generated pallette contains exposed function"() {
        setup:
            
            def text = genTemplate("src/main/resources/templates/pallette.tmpl.gsp", [])
            println text
        expect:
            text.contains("module Pallette exposing (pallette)")
            text.contains("pallette model =")

    }

}