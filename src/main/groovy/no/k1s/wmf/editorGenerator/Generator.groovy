package no.k1s.wmf.editorGenerator

import groovy.text.*

import no.k1s.wmf.parser.XmiParser

class Generator {
    def parser = new XmiParser()


    def generateElmCode(String modelXmi, String outputDir, String modelElement){
        def model = parser.parse(modelXmi) //parsing with eclipse
        new File("${outputDir}/src").mkdir()
        generateTypes(model, outputDir, modelElement)
        generatePallette(model, outputDir)
        generateEditor(model, outputDir, modelElement)
        generateAttributes(model, outputDir, modelElement)
        generateMain(model, outputDir)
        generateUpdate(model, outputDir, modelElement)
        generateApp(model, outputDir, modelElement)

        generateStaticContent(outputDir)
    }

    def generateStaticContent(outputDir){
        def index =  runTemplate("./src/main/resources/templates/index.tmpl.html",null)
        new File("${outputDir}/index.html").write index

        def js = runTemplate("./src/main/resources/templates/indexjs.tmpl.js",null)
        new File("${outputDir}/index.js").write js

        def logo = runTemplate("./src/main/resources/templates/logo.tmpl.svg",null)
        new File("${outputDir}/logo.svg").write logo

        def css = runTemplate("./src/main/resources/templates/main.tmpl.css",null)
        new File("${outputDir}/main.css").write css

        def packageJson = runTemplate("./src/main/resources/templates/elm.tmpl.json",null)
        new File("${outputDir}/elm.json").write packageJson
    }


    def generateApp(model, outputDir, modelElement){
        def main =  runTemplate("./src/main/resources/templates/app.tmpl.elm",[ecModel: model, modelElement:modelElement])
        new File("${outputDir}/src/App.elm").write main
    }

    def generateUpdate(model, outputDir, modelElement){
        def main =  runTemplate("./src/main/resources/templates/update.tmpl.gsp",[ecModel: model, modelElement:modelElement])
        new File("${outputDir}/src/Update.elm").write main
    }

    def generatePallette(model, outputDir){
        def main =  runTemplate("./src/main/resources/templates/pallette.tmpl.gsp",null)
        new File("${outputDir}/src/Pallette.elm").write main
    }

    def generateTypes(model, outputDir, modelElement){
        def types =  runTemplate("./src/main/resources/templates/types.tmpl.gsp", [ecModel: model, modelElement:modelElement])
        new File("${outputDir}/src/Types.elm").write types
    }

    def generateMain(model, outputDir){
        def main = runTemplate("./src/main/resources/templates/main.tmpl.gsp",null)
        new File("${outputDir}/src/Main.elm").write main
    }

    def generateEditor(model, outputDir, modelElement){
        def main = runTemplate("./src/main/resources/templates/editor.tmpl.elm", [ecModel: model, modelElement:modelElement])
        new File("${outputDir}/src/Editor.elm").write main
    }
    
    def generateAttributes(model, outputDir, modelElement){
        def main = runTemplate("./src/main/resources/templates/attributes.tmpl.elm", [ecModel: model, modelElement:modelElement])
        new File("${outputDir}/src/Attributes.elm").write main
    }

    def runTemplate(templatePath, params){
    def retval = null
     try{
       def tmpl = new File(templatePath).text
       SimpleTemplateEngine engine = new SimpleTemplateEngine()
       Template template = engine.createTemplate(tmpl)
       if(params == null || params.size == 0)
           retval = template.make().toString()
       else
           retval = template.make(params).toString()
     } catch(Exception e){
       throw new RuntimeException("Exception running template: $templatePath with $params", e)
     }
     return retval
  }
}
