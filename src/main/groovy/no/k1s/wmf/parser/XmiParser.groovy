package no.k1s.wmf.parser


import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.*
import org.eclipse.emf.ecore.xmi.impl.*
import  org.eclipse.emf.common.util.URI

class XmiParser {
    
    def parse(File file){
        URI uri = URI.createFileURI(file.absolutePath)
 
        Resource resource = new XMIResourceImpl()
     
        resource.unload()
        resource.setURI(uri)
        
        try {
            resource.load(null);
             
            //XMIModel : Create a class for XMI Model
            def model = resource.getContents().get(0)
             return model
            // //Iterate on the XMI Model or Convert it to a tree
            // TreeViewer treeViewer;
            // treeViewer.setInput(model);
 
        } catch (IOException e) {
            System.err.println(e.getMessage())
            throw e
        }
    }

    def parse(String xmiModel){
        
        Resource resource = new XMIResourceImpl()
        resource.unload()
        
        try {
            resource.load(new ByteArrayInputStream(xmiModel.getBytes()), null)
            def model = resource.getContents().get(0)
            return model
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(e.getMessage())
            throw e
        }
    }
}
