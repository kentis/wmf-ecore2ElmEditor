module Types exposing (..)
import Browser
import Dict exposing (Dict)
import Json.Decode as Decode

type alias Position = 
    { x : Int
    , y : Int
    }

type alias Model =
    { model : ModelDef
    , editor : EditorDef
    , pallette: PalletteDef
    , selected: Int
    , dragged: Int
    , draggedBp: Int
    , dragType : DragType
    , drag : Maybe Drag
    , resized: Int
    , resize : Maybe ReSize
    , arcStart : Maybe Int
    , nextId : Int
    }

<% ecModel.getEClassifiers().each { %>
type alias Node_${it.name} = 
    { id : Int             
    , isSelected : Bool             
    , label : String             
    , width : Int             
    , x : Int             
    , y : Int             
    , height : Int
    , elementType: ElementType
    , shape: ElementShape
    , attributes: Dict String String
    }
<% } %>

type alias NodeDef = 
    { id : Int             
    , isSelected : Bool             
    , label : String             
    , width : Int             
    , x : Int             
    , y : Int             
    , height : Int
    , elementType: ElementType
    , shape: ElementShape
    , attributes: Dict String String
    }

type ElementShape = 
      Circle
    | Rectangle
    | Custom

type ElementType =
    <% ats = [] %>
    <% ecModel.getEClassifiers().eachWithIndex{ it, i -> %>
        ${i>0?"| ":""}ET_${it.name}
        <%it.getEReferences().each {ref ->%>
        ${ats.contains(ref.name) ? "" : "|AT_"+ref.name }
        <% ats << ref.name %>
    <% }} %>

type alias ModelDef = {
<% ecModel.getEClassifiers().eachWithIndex{ it, i -> %>
    ${i>0?", ":""}n${it.name}List : List Node_${it.name}
<% } %>
    , arcs : List ArcDef
    }

type alias EditorDef =
  { x: Int
  , y: Int
  , width : Int
  , height: Int
  }

type alias PalletteDef = 
  { x: Int
  , y: Int
  , width : Int
  , height: Int
  , elements : List PalletteElement
  , arcElements : List PalletteArcElement
  }

type alias ArcDef =
  { id : Int
  , source : Int
  , target : Int
  , isSelected: Bool
  , breakPoints: List BreakPoint
  , elementType: ElementType
  }

type alias BreakPoint =
  { id: Int
  , x: Int
  , y: Int
  }

type DragType =
   DRAG_NODE
  |DRAG_BREAKPOINT
  |NONE


type alias Drag = {}

type alias ReSize = 
    { id: Int
    , startX : Int
    , startY : Int
    }

type alias PalletteElement = 
    { id: Int
    , text: String
    , selected: Bool
    , tool: Tool
    , elementType: ElementType
    , shape: ElementShape
    , attributes: Dict String String
    }

type alias PalletteArcElement = 
    { id: Int
    , text: String
    , selected: Bool
    , tool: Tool
    , elementType: ElementType
    , sourceType: ElementType
    , targetType: ElementType
    }

type alias Point = 
    { x: Float
    , y: Float
    }

type Tool =
    CREATE_NODE
    | CREATE_ARC

type Msg
    = NoOp
    | Fire Int
    | ClickNode Int
    | DragStart Int Position
    | DragAt DragType Position
    | DragEnd Position
    | DragBreakPointStart Int Int Position
    | DragBreakPointAt Position
    | DragBreakPointEnd Position
    | ResizeStart Int Position
    | ResizeAt Position
    | ResizeEnd Position
    | ApplyTool (Maybe Int) Position
    | SelectElement Int
    | SelectArc Int
    | ChangeLabel Int String String
    | AddBreakPoint Int Position 

positionDecoder : Decode.Decoder Position
positionDecoder =
  Decode.map2 Position
    (Decode.field "x" Decode.int)
    (Decode.field "y" Decode.int)