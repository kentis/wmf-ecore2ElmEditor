module Editor exposing (renderEditor)
import Html exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Html.Events exposing (..)
import Update exposing (..)
import Browser
import Json.Decode as Decode
import Types exposing (..)
import String exposing (fromInt, fromFloat)


renderEditor : Model -> Html.Html Msg 
renderEditor model = 
    svg [
        width (fromInt model.editor.width)
        , height (fromInt model.editor.height)
        , Svg.Attributes.style (" position: absolute; left: "++ (fromInt model.editor.x) ++"px; top:"++(fromInt model.editor.y) ++"px;   ")
        , viewBox "0 0 1200 1000"
        , onEditorClick Nothing] 
        ([ 
          marker [id "arrow", markerWidth "10", markerHeight "10", refX "8", refY "3", orient "auto", markerUnits "strokeWidth"] [
            Svg.path [d "M0,0 L0,6 L9,3 z", fill "red"] []
          ]
         ] <% ecModel.getEClassifiers().each { %>
         ++ (List.concat (List.map renderNode model.model.n${it.name}List ))
        <% } %>
        ++ (List.concat (List.map ((renderArc model) ( <% ecModel.getEClassifiers().each { %> model.model.n${it.name}List ++ <% } %> [])) model.model.arcs ))
        )


<% ecModel.getEClassifiers().each { %>
render${it.name} node = 
    ${it.getEAnnotation("render:Circle") != null ? "renderAsCircle node" :"renderAsRect node"}
<% } %>

renderNode node =
  case node.shape of
      Circle ->
        renderAsCircle node
      Rectangle ->
        renderAsRect node
      Custom -> 
        renderAsRect node


renderArc : Model -> List NodeDef -> ArcDef -> List (Svg.Svg Msg)
renderArc model nodes arc =
  let
    startNode = case (List.filter (\\x -> x.id == arc.source) nodes) of
                    n :: rest ->
                      Just n
                    option2 ->
                      Nothing
    targetNode = case (List.filter (\\x -> x.id == arc.target) nodes) of
                    n :: rest ->
                      Just n
                    option2 ->
                      Nothing
  in
      case startNode of
            Just sn ->
              case targetNode of
                Just tn ->
                  [
                    --line [x1 (fromInt sn.x), y1 (fromInt sn.y),  x2 (fromInt tn.x), y2 (fromInt tn.y), Svg.Attributes.style "stroke:rgb(255,0,0);stroke-width:2", markerEnd "url(#arrow)", onArcClick arc.id] []
                    Svg.path [d (renderPath model sn tn arc.breakPoints)
                             , stroke "red"
                             , strokeWidth "2"
                             , fill "none"
                             , Svg.Attributes.style "stroke:rgb(255,0,0);stroke-width:2"
                             , markerEnd "url(#arrow)"
                             , onArcClick arc.id 
                             ][]
                  ]
                  ++ if arc.isSelected then
                        [
                          --line [x1 (fromInt sn.x), y1 (fromInt sn.y), x2 (fromInt tn.x), y2 (fromInt tn.y),
                          --     Svg.Attributes.style "stroke:rgb(0,0,0);stroke-width:3", onAddBreakPoint arc.id] []
                          Svg.path [d (renderPath model sn tn arc.breakPoints), stroke "balck", strokeWidth "3", fill "none", Svg.Attributes.style "stroke:rgb(0,0,0);stroke-width:3", onAddBreakPoint arc.id ][]
                        ] ++ (renderBreakPoints model arc arc.breakPoints)
                      else
                        []
                Nothing ->
                  []
            Nothing ->
              []

renderArcStart: NodeDef -> NodeDef -> List BreakPoint -> String
renderArcStart node endNode breakPoints = 
  case node.shape of
      Circle ->      
        circleIntersect node endNode breakPoints
      Rectangle ->
        rectangleIntersect node endNode breakPoints
        --(fromInt ((toFloat node.x) + ((toFloat node.width)/2))) ++ " " ++ (fromInt ((toFloat node.y) + ((toFloat node.height)/2)))
      Custom -> 
        rectangleIntersect node endNode breakPoints
        --(fromInt ((toFloat node.x) + ((toFloat node.width)/2))) ++ " " ++ (fromInt ((toFloat node.y) + ((toFloat node.height)/2)))

circleIntersect: NodeDef -> NodeDef -> List BreakPoint -> String
circleIntersect node endNode breakPoints =
  let
    centerStart = getCenter node
    centerNext = case List.head breakPoints of
        Just bp ->
          {x = toFloat bp.x, y = toFloat bp.y}
        Nothing ->
            getCenter endNode
    radius = ((toFloat (node.width + node.height)/4))
    d = sqrt ( (centerStart.x - centerNext.x)^2 + (centerStart.y - centerNext.y)^2 )
  in
    fromFloat (centerStart.x - ( ( radius * (centerStart.x - centerNext.x) ) / d))
    ++ " "
    ++ fromFloat (centerStart.y - ( ( radius * (centerStart.y - centerNext.y) ) / d))

rectangleIntersect: NodeDef -> NodeDef -> List BreakPoint -> String
rectangleIntersect node endNode breakPoints =
  let
      centerStart = getCenter node
      centerNext = case List.head breakPoints of
          Just bp ->
            {x = toFloat bp.x, y = toFloat bp.y}
          Nothing ->
              getCenter endNode
      m = getM centerStart centerNext --|> Debug.log "m"
      b = getB centerStart m --|> Debug.log "b"
      topLeft = {x = toFloat node.x, y = toFloat node.y}
      bottomLeft = {x = toFloat node.x, y = toFloat (node.y + node.height)}
      topRight = {x = toFloat (node.x + node.width), y = toFloat node.y}
      bottomRight = {x = toFloat (node.x + node.width), y = toFloat (node.y + node.height)}
      m1 = getM topLeft bottomLeft --|> Debug.log "m1"
      m2 = getM topLeft topRight --|> Debug.log "m2"
      m3 = getM topRight bottomRight  --|> Debug.log "m3"
      m4 = getM bottomLeft bottomRight --|> Debug.log "m4"
      b1 = getB topLeft m1  --|> Debug.log "b1"
      b2 = getB topLeft m2 --|> Debug.log "b2"
      b3 = getB topRight m3 --|> Debug.log "b3"
      b4 = getB bottomLeft m4 --|> Debug.log "b4"
      intersect = lineIntersect m b
      dst = pointDistance centerNext
  in
    case [lineIntersect m b m1 b1 centerStart, lineIntersect m b m2 b2 centerStart, lineIntersect m b m3 b3 centerStart, lineIntersect m b m4 b4 centerStart] 
    --|> Debug.log "intersections"
    |> maybeValues
    --|> Debug.log "intersections"
    |> List.filter (filterPoints node)
    --|> Debug.log "filtered intersections"
    |> List.sortBy dst
    |> Debug.log "sorted intersections"
    |> List.head of
        Just point ->
            (fromFloat point.x) ++ " " ++ (fromFloat point.y)
        Nothing ->
            (fromInt node.x) ++ " " ++ (fromInt node.y)


filterPoints: NodeDef -> Point -> Bool
filterPoints node point =
  let
      x1 = toFloat node.x
      y1 = toFloat node.y
      x2 = (toFloat node.x) + (toFloat node.width)
      y2 = toFloat node.y
      x3 = toFloat node.x
      y3 = toFloat node.y + (toFloat node.height)
      x4 = (toFloat node.x) + (toFloat node.width)
      y4 = toFloat node.y + (toFloat node.height)
  in
  (point.x >= x1 && point.x <= x2 &&
    point.y >= y1 && point.y <= y2 )
  ||
  (point.x >= x1 && point.x <= x3 &&
    point.y >= y1 && point.y <= y3 )
  ||
  (point.x >= x3 && point.x <= x4 &&
    point.y >= y3 && point.y <= y4 )
  ||
    (point.x >= x2 && point.x <= x4 &&
    point.y >= y2 && point.y <= y4 )

pointDistance: Point -> Point -> Float
pointDistance p1 p2 =
  abs (sqrt ( (p1.x - p2.x)^2 + (p1.y - p2.y)^2 ) )

lineIntersect: Float -> Float -> Float -> Float -> Point -> Maybe Point
lineIntersect m1 b1 m2 b2 p =
  case isInfinite m2 of
    True ->
      Just {x = b2, y=(m1*p.x + b1)}
    False ->
      case round ((m1 - m2) * 10.0) of
          0 ->
            Nothing
          _ ->
            let
              x = (b2 - b1) / (m1 - m2)
              y = m1 * ((b2 - b1) / (m1 - m2)) + b1
            in
              Just {x = abs x, y = abs y}

getB: Point -> Float -> Float
getB p1 m = -- b = y - mx
  case isInfinite m of
      True ->
        p1.x
      False ->
        p1.y - (m * p1.x)

getM: Point -> Point -> Float
getM p1 p2 =
        (p2.y - p1.y) / (p2.x - p1.x)


getCenter: NodeDef -> Point
getCenter node =
     case node.shape of
      Circle ->
        {x = toFloat node.x, y = toFloat node.y}
      _ ->
        {x = (toFloat node.x) + ((toFloat node.width)/2), y = (toFloat node.y) + ((toFloat node.height)/2)}

renderPath: Model -> NodeDef -> NodeDef -> List BreakPoint -> String
renderPath model startNode endNode breakPoints =
    "M"
    ++ renderArcStart startNode endNode breakPoints
    ++  (case breakPoints of
        [] ->
          ""
        _ ->
          " L" ++  String.join " L" (List.map  (\\bp -> fromInt (bp.x - model.editor.x) ++ " " ++ fromInt (bp.y - model.editor.y)) breakPoints))
    ++" L"++renderArcStart endNode startNode (List.reverse breakPoints)

renderBreakPoints : Model -> ArcDef  -> List BreakPoint -> List (Svg.Svg Msg)
renderBreakPoints model arc breakPoints =
  List.map (\\bp -> circle [cx (fromInt (bp.x - model.editor.x)), cy (fromInt (bp.y - model.editor.y)), r "4", onMouseDownBreakPoint arc.id bp.id][]) breakPoints



renderAsCircle node =
  let
    radius = ((toFloat (node.width + node.height)/4))
  in
    [
      Svg.text_ [
        textAnchor "middle", x (fromInt node.x)
        , y (fromInt node.y)
        , width (fromInt (node.width - 2))] [Svg.text node.label]
        , circle [cx (fromInt node.x), cy (fromInt node.y), r (fromFloat radius), stroke "black", strokeWidth "1", fillOpacity "0", onClick (ClickNode node.id), onEditorClick (Just node.id), onMouseDown node.id] []
    ]
    ++ case node.isSelected of
        True ->
          [ circle [cx (fromInt node.x), cy (fromInt node.y), r (fromFloat ((toFloat ((node.width + node.height))/4) +2)), stroke "black", strokeWidth "1", fillOpacity "0", strokeDasharray "2"][]
          , rect [x (fromInt (node.x + (ceiling radius) )), y (fromInt (node.y + (ceiling radius))), width "4", height "4", stroke "black", strokeWidth "1", onResizeMouseDown node.id][]
          ] 
        False -> []

renderAsRect node = 
  [rect [x (fromInt node.x), y (fromInt node.y), width ((fromInt node.width)++"px"), height (fromInt node.height), fill "white", stroke "black", strokeWidth "1",  onEditorClick (Just node.id), onMouseDown node.id] []
    ,Svg.text_ [
      textAnchor "middle", x (fromFloat ((toFloat node.x) +  ((toFloat node.width) / 2)))
      , y (fromFloat ((toFloat node.y) +  ((toFloat node.height) / 2)))
      , width (fromInt (node.width - 2))] [Svg.text node.label]]
    ++ case node.isSelected of
        True -> 
          [ rect [x (fromInt (node.x - 2)), y (fromInt (node.y - 2)), width ((fromInt (node.width + 4))++"px"), height (fromInt (node.height+ 4)), stroke "black", strokeWidth "1", fillOpacity "0", strokeDasharray "2"] []
          , rect [x (fromInt (node.x + node.width + 2)), y (fromInt (node.y + node.height + 2)), width "4", height "4", stroke "black", strokeWidth "1", onResizeMouseDown node.id][]
          ]
        False -> []




onMouseDown : Int -> Html.Attribute Msg
onMouseDown id = 
    on   "mousedown" (Decode.map (DragStart id) positionDecoder)

onMouseDownBreakPoint : Int -> Int -> Html.Attribute Msg
onMouseDownBreakPoint arcId bpId=
    on "mousedown" (Decode.map (DragBreakPointStart arcId bpId) positionDecoder)

onAddBreakPoint:  Int -> Html.Attribute Msg
onAddBreakPoint id =
  stopOn "click" (Decode.map (AddBreakPoint (Debug.log "add-bp" id)) positionDecoder)

onArcClick:  Int -> Html.Attribute Msg
onArcClick  id =
    stopOn "click" (Decode.succeed (SelectArc (Debug.log "arc-click id" id) ))

onEditorClick:  Maybe Int -> Html.Attribute Msg
onEditorClick  id = -- {stopPropagation=True, preventDefault=True}
    stopOn "click"  (Decode.map (ApplyTool id) positionDecoder)

stopOn : String -> Decode.Decoder msg -> Html.Attribute msg
stopOn event =
    Decode.map (\\msg -> { message = msg, stopPropagation = True, preventDefault = True })
      >> Html.Events.custom event

onResizeMouseDown id =
  on "mousedown" (Decode.map (ResizeStart id) positionDecoder)
-- onMouseUp =
--     on "mouseup" (Decode.map DragEnd Position)

-- Utility functions 
maybeValues : List (Maybe a) -> List a
maybeValues =
  List.foldr foldrValues []


foldrValues : Maybe a -> List a -> List a
foldrValues item list =
  case item of
    Nothing ->
      list
    Just v ->
      v :: list