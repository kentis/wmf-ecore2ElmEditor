module Attributes exposing (attributeView)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Types exposing (..)
import String exposing (fromInt)
import Dict
import List
import Debug 

attributeView : Model -> Html.Html Msg 
attributeView model = 
    table [id "attributes"
            , Html.Attributes.style "position" "absolute"
            , Html.Attributes.style "left" ((fromInt (model.editor.x + model.editor.width))++"px")
            , Html.Attributes.style "top" ((fromInt model.editor.y) ++"px")
            , Html.Attributes.style "border-left" "solid"
            , Html.Attributes.style "heigth" "100%"
        ] (getAttributeRows model)

getAttributeRows: Model -> List (Html Msg)
getAttributeRows model = 
    let
      selectedNode = List.head (List.filter (\\x -> x.isSelected == True) ([] <% ecModel.getEClassifiers().each { %>++ model.model.n${it.name}List <% } %>|>Debug.log "attr nodes") )
    in
        case selectedNode |> Debug.log "selectedNode" of
          Just node ->
            getAttributesForNode node model
          Nothing ->
            []

getAttributesForNode: NodeDef -> Model -> List (Html Msg)
getAttributesForNode node model =
    [
        tr [] [ td [][text "Id"]
            , td [] [text (fromInt node.id)]
        ]
    ]
    ++  List.map (\\k -> tr [] [
          td [][ text k]
          ,td [] [
            input [value (case Dict.get k node.attributes of
                            Just v ->
                                v
                            _ ->
                                ""), onInput (ChangeLabel node.id k) ] []    
          ] ]) (Dict.keys node.attributes)

        -- , tr [] ([
        -- td [][ text "Label"]
        -- ,td [] [
        --       input [value node.label, onInput (ChangeLabel node.id "label") ] []    
        -- ] 