module App exposing (..)
import Types exposing (..)
import Dict exposing (Dict)
import Pallette exposing (..)
import Html exposing (Html, div)
import Editor exposing (renderEditor)
import Update exposing (..)
import Browser
import Attributes exposing (attributeView)
    
init : () -> ( Model, Cmd Msg )
init _ =
    ( {  
    model = {
        <% ecModel.getEClassifiers().eachWithIndex{ it, i -> %>
        ${i>0?", ":""}n${it.name}List = []
        <% } %>
        ,arcs = []
    }
    , editor = {x = 250, y = 50, height=1000, width = 1200}
    , pallette = {x=0, y=50, height=1000, width=250, elements = [
        <% def isFirst = true
        ecModel.getEClassifiers().eachWithIndex{ it, i -> 
            if(it.name != modelElement){%>
                ${isFirst ? "" : ","}{
                    id = $i
                    , attributes = Dict.fromList [<% it.eContents().findAll({p -> p instanceof org.eclipse.emf.ecore.EAttribute}).eachWithIndex{ prop, propId-> %>
                        ${propId != 0 ? ",":""}("${prop.name}", "")
                    <% } %>]
                    , text = "${it.name}"
                    , selected = False
                    , tool = CREATE_NODE
                    , elementType = ET_${it.name}
                    , shape = ${it.getEAnnotation("render:Circle") != null ? "Circle" : "Rectangle"}
                }<%  isFirst = false
            }} %>
        ]
        , arcElements = [
            <%  isFirst = true
            def count = 1000
            ecModel.getEClassifiers().eachWithIndex{ it, i -> 
            if(it.name != modelElement){
                it.getEReferences().each {ref ->
                %>
                ${isFirst ? "" : ","}{
                    id = ${++count}
                    , text = "${ref.name}"
                    , selected = False
                    , tool = CREATE_ARC
                    , elementType = AT_${ref.name}
                    , sourceType = ET_${it.name}
                    , targetType = ET_${ref.eType.name}
                }
        <%  isFirst = false
            }}} %>
        ] 
    }
    , selected = 0
    , dragged = 0
    , drag = Nothing
    , draggedBp = 0
    , dragType = NONE
    , resized = 0
    , resize = Nothing
    , arcStart= Nothing
    , nextId = 1
    }, Cmd.none )

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    Update.update msg model


view : Model -> Html Msg
view model =
    div []
        [ 
          pallette model
        , renderEditor model
        , attributeView model
        ]

