module Pallette exposing (pallette)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Types exposing (..)
import String exposing (fromInt)

pallette: Model -> Html Msg
pallette model =
    ul [ (style "position" "absolute")
       , (style "left" ((fromInt model.pallette.x)++"px"))
       , (style "top"  ((fromInt model.pallette.y)++"px"))
       , (style "width" ((fromInt model.pallette.width)++"px"))
       , (style "height" ((fromInt model.pallette.height)++"px"))
       , (style "margin" "0")
       , (style "padding" "0")
       , (style "text-align" "left")
       , (style "border-right" "solid")
       ]
       ((List.map renderElement model.pallette.elements)
       ++ [hr[][]]
       ++ (List.map renderArcElement (getUniqueElements model.pallette.arcElements [])))

renderElement : PalletteElement -> Html Msg
renderElement element =
    li [(getElementStyle element.selected), onClick (SelectElement element.id) ] [ text element.text ]


renderArcElement : PalletteArcElement -> Html Msg
renderArcElement element =
    li [(getElementStyle element.selected), onClick (SelectElement element.id) ] [ text element.text ]


getElementStyle : Bool -> Attribute Msg
getElementStyle selected =
        style "background-color"  (case selected of
                    True ->
                        "gray"
                    _ ->
                        "white")

getUniqueElements: List PalletteArcElement -> List PalletteArcElement -> List PalletteArcElement
getUniqueElements elements retval =
  case elements of
    (head::rest) ->
      case List.filter (\\x -> x.text == head.text ) retval of
        [] -> 
          getUniqueElements rest (head::retval)
        _ ->
          getUniqueElements rest retval
    [] ->
      retval
