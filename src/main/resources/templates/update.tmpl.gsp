module Update exposing (update, subscriptions)
import Browser
import Browser.Events as Events
import Dict exposing(Dict)
import Types exposing (..)
import Json.Decode as Decode


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
      ret = 
        case msg|>Debug.log "msg" of
          DragStart id position->
            ({model | dragged = id, drag = (Just {}), dragType = DRAG_NODE}, Cmd.none)
          DragBreakPointStart id bpid position->
            ({model | dragged = id, drag = (Just {}), draggedBp=bpid, dragType = DRAG_BREAKPOINT}, Cmd.none)
          DragAt dragType position ->
            case model.drag of
              Just drag ->
                let
                  modelDef = model.model
                in
                  case model.dragType of
                    DRAG_NODE ->
                      (updatePosition model position drag, Cmd.none)
                    DRAG_BREAKPOINT ->
                      ({model | model=( {modelDef| arcs= (updateBpPosition model position drag)} )}, Cmd.none)
                    NONE ->
                      ( model, Cmd.none )
              Nothing ->
                ( model, Cmd.none )
          DragEnd pos ->
            ({model | drag = Nothing, dragType = NONE }, Cmd.none)
          ResizeStart id pos ->
                ({model | resized = id, resize=(Just {id=id, startX = pos.x, startY = pos.y})}, Cmd.none)
          ResizeAt pos -> ( 
            let 
              newModel = (applyResize model pos)
              id = case model.resize of
                      Just resize ->
                        resize.id
                      Nothing ->
                        -1
            in
              {newModel| resize=(Just {id=id, startX = pos.x, startY = pos.y})}, Cmd.none)
          ResizeEnd pos ->
            ({model | resize = Nothing }, Cmd.none)
          SelectElement id ->
              ({model | pallette = (updatePallette model.pallette id)}|>deselectAll, Cmd.none )
          ApplyTool elemId pos ->
              ((applyTool model pos elemId), Cmd.none)
          ChangeLabel id attrib value ->
            (changeLabel model id attrib value, Cmd.none)
          SelectArc id ->
            ({model | model=(selectArc model.model id)}, Cmd.none)
          AddBreakPoint id pos ->
            let 
              modelDef = model.model
            in
              ({model | model={modelDef | arcs=(addBreakPoint id pos modelDef.arcs)}}, Cmd.none)

          _ ->
              ((deselectAll model), Cmd.none)
    in
      Debug.log "model" ret

subscriptions : Model -> Sub Msg
subscriptions model =
  case model.drag of
    Nothing ->
      case model.resize of
        Just _ ->
          Sub.batch [ Events.onMouseMove (Decode.map ResizeAt positionDecoder)
                    , Events.onMouseUp (Decode.map ResizeEnd positionDecoder)
                    ]
        Nothing ->
          Sub.none
    Just _ ->
      Sub.batch [ Events.onMouseMove (Decode.map (DragAt model.dragType) positionDecoder)
                , Events.onMouseUp (Decode.map DragEnd positionDecoder) 
                ]

selectArc: ModelDef -> Int -> ModelDef
selectArc modelDef id =
  {modelDef | arcs=(selectArcs modelDef.arcs id)}

selectArcs arcs id =
  List.map (\\x -> 
    case x.id == id of
      True ->
        {x| isSelected=True}
      _ ->
        {x| isSelected=False}
    ) arcs

<% 
ecModel.getEClassifiers().each{ it -> %>
updatePosition${it.name}IfMatch : Model -> Position -> Int -> List Node_${it.name} -> List Node_${it.name}
updatePosition${it.name}IfMatch model position id nodes =
  List.map (\\node -> case node.id == id of
      True ->
        {node | x = position.x - model.editor.x, y = position.y  - model.editor.y}
      False ->
        node
    ) nodes
<% } %>
 
updatePosition: Model -> Position -> Drag -> Model
updatePosition model position drag =
  let
    modeldef = model.model
  in
    {model | model={modeldef | 
      <% ecModel.getEClassifiers().eachWithIndex{ it, i -> %>
      ${i>0?", ":""} n${it.name}List = (updatePosition${it.name}IfMatch model position model.dragged modeldef.n${it.name}List)
      <% } %>
    } }
--    model.model.nodes
--    |> List.map (updateNodeIfMatch model.dragged position model)

--updateNodeIfMatch: Int -> Position -> Model -> Node -> Node
--updateNodeIfMatch id position model node =
--    case node.id == id of
--      True ->
--        {node | x = position.x - model.editor.x, y = position.y  - model.editor.y}
--      False ->
--        node


applyTool: Model -> Position -> Maybe Int -> Model
applyTool model pos elemId =
  let
    tool = getToolFromPallette model.pallette
    modeldef = model.model
  in
        case tool of
          Just CREATE_NODE ->
            addNewNodeByTool model pos
          Just CREATE_ARC ->
              case elemId of
                Just id ->
                  case model.arcStart of
                    Just startId->
                      let 
                        palletteElement = List.head (List.filter (\\x -> x.selected ) model.pallette.arcElements)
                        startNode = List.head (List.filter (\\x -> x.id == startId ) (allNodes modeldef) )
                        targetNode = List.head (List.filter (\\x -> x.id == id ) (allNodes modeldef) )
                      in
                        case (palletteElement, startNode, targetNode) of
                          (Just pe, Just sn, Just tn) ->
                            if sn.elementType == pe.sourceType && tn.elementType == pe.targetType then
                              {model | arcStart = Nothing,
                                    pallette = (updatePallette model.pallette -1),
                                    model={modeldef | arcs=model.model.arcs++[{id=(getNextId model.model.arcs),  source=startId, target=id, isSelected=False, breakPoints=[], elementType=pe.elementType}] } }
                            else
                              {model | arcStart = Nothing}
                          _ ->
                            {model | arcStart = Nothing} 
                    Nothing ->
                      {model | arcStart = (Just id)}
                Nothing ->
                  {model | pallette = (updatePallette model.pallette -1)}
          Nothing -> --No tool means select element
            case elemId of
              Just id ->
                {model | model={modeldef | 
                  <% ecModel.getEClassifiers().eachWithIndex{ it, i -> %>
                    ${i>0?", ":""}n${it.name}List = (select${it.name}IfMatch id modeldef.n${it.name}List)
                  <% } %>
                } } 
              _ ->
                deselectAll model

addNewNodeByTool: Model -> Position -> Model
addNewNodeByTool model pos =
  let
    modeldef = model.model
    tool = List.head <|List.filter (\\x -> x.selected) model.pallette.elements
    nextId = model.nextId
  in
    case tool of
      Just t ->
        let
          newNode = {
            id=nextId, 
            label="", 
            x=pos.x-model.editor.x, 
            y=pos.y-model.editor.y, 
            width=10, 
            height=10, 
            elementType=t.elementType,
            isSelected = False, 
            shape = t.shape,
            attributes = t.attributes
            }
        in
          case t.elementType of
            <%ats = []; ecModel.getEClassifiers().each{ it -> %>
            ET_${it.name} ->
              {model | nextId=(nextId + 1), model={modeldef | n${it.name}List=newNode :: modeldef.n${it.name}List}, pallette=(updatePallette model.pallette -1)}
              
              <%it.getEReferences().each {ref ->%>
            ${ats.contains(ref.name)?"":"AT_"+ref.name+" ->\n              model"}
            <% ats << ref.name
}} %>     |> deselectAll
      _ ->
       deselectAll model

changeLabel : Model -> Int -> String -> String -> Model
changeLabel model id attrib value =
  let
    modelDef = model.model
    arcs = model.model.arcs
    updatel = (updateLabel id attrib value)
  in
    {model | model={modelDef| arcs=arcs  <%ecModel.getEClassifiers().each { %>
      ,n${it.name}List=(updatel modelDef.n${it.name}List)<% } %>
    }}

updateLabel: Int -> String -> String -> (List NodeDef) -> (List NodeDef)
updateLabel id key value nodes =
  List.map (\\x -> case x.id == id of
                      True ->
                        {x| attributes=(Dict.insert key value x.attributes)}
                      _ ->
                        x
           ) nodes

deselectAll: Model -> Model
deselectAll model = 
  let
      modeldef = model.model
      arcs = model.model.arcs
  in
    {model | selected = -1, model={modeldef | arcs=(selectArcs arcs -1)  <%ecModel.getEClassifiers().each { %>
      ,n${it.name}List=(select${it.name}IfMatch -1 modeldef.n${it.name}List)<% } %>
      } 
    }

<% 
ecModel.getEClassifiers().each{ it -> %>
select${it.name}IfMatch : Int -> List Node_${it.name} -> List Node_${it.name}
select${it.name}IfMatch id nodes =
  List.map (\\x -> {x | isSelected = x.id == id}) nodes
<% } %>

--selectNodeIfMatch: Int -> Node -> Node
--selectNodeIfMatch id node=
--  {node | isSelected = (node.id == id) }


getToolFromPallette: PalletteDef -> Maybe Tool
getToolFromPallette pallette =
    let
      element = List.head <| List.filter (\\x -> x.selected) pallette.elements
     in
        case element of
          Just e ->
            Just e.tool
          Nothing ->
            let 
              arcElement =  List.head <| List.filter (\\x -> x.selected) pallette.arcElements
            in
              case arcElement of
                Just ae ->
                  Just ae.tool
                Nothing ->
                  Nothing

updatePallette pallette id =
  {pallette | elements=(List.map (\\e-> {e | selected = (e.id == id)}) pallette.elements), 
              arcElements=(List.map (\\e-> {e | selected = (e.id == id)}) pallette.arcElements)}

updateBpPosition: Model -> Position -> Drag -> List ArcDef
updateBpPosition model position drag =
    model.model.arcs
    |> List.map (updateArcBpIfMatch model.dragged model.draggedBp position model)
    

updateArcBpIfMatch: Int ->  Int -> Position -> Model -> ArcDef -> ArcDef
updateArcBpIfMatch arcId bpId pos model arc =
  case arcId == arc.id of
     True ->
      {arc | breakPoints=(List.map (updateBpIfMAtch bpId pos model) arc.breakPoints)}
     _ ->
      arc

updateBpIfMAtch: Int -> Position -> Model -> BreakPoint -> BreakPoint
updateBpIfMAtch bpId pos model bp =
  case bp.id == bpId of
    True ->
      {bp | x = pos.x, y = pos.y}
    _ ->
      bp

addBreakPoint: Int -> Position -> List ArcDef -> List ArcDef
addBreakPoint id pos arcs= 
    List.map (\\x -> case x.id == id of
                      True ->
                          {x | breakPoints=(newBreakPoint pos x.breakPoints )::x.breakPoints}
                      False ->
                        x
              ) arcs
        

newBreakPoint: Position -> List BreakPoint -> BreakPoint
newBreakPoint position  breakPoints =
  let 
    nextId = case List.maximum (List.map (\\x -> x.id) breakPoints) of
                Just n ->
                  n +1
                Nothing ->
                  1            
  in 
    {id=nextId, x=(position.x), y=(position.y)}


<% 
ecModel.getEClassifiers().each{ it -> %>
updateSize${it.name}IfMatch : Model -> Position -> ReSize -> Int -> List Node_${it.name} -> List Node_${it.name}
updateSize${it.name}IfMatch model position resize id nodes =
  List.map (\\node -> case node.id == id of
      True ->
        {node | width = (node.width + (position.x - resize.startX)),  height=(node.height + (position.y - resize.startY))}
      False ->
        node  
    ) nodes
<% } %>

applyResize: Model -> Position ->  Model
applyResize model pos  =
  case model.resize of
    Just resize ->
      let
        modeldef = model.model
      in
        {model | model={modeldef | 
          <% ecModel.getEClassifiers().eachWithIndex{ it, i -> %>
          ${i>0?", ":""} n${it.name}List = (updateSize${it.name}IfMatch model pos resize model.resized modeldef.n${it.name}List)
          <% } %>
        } }
    Nothing ->
      model
  --model.model.nodes
  --  |> List.map (updateNodeSizeIfMatch model.resized pos resize model)

allNodes model =
<%ecModel.getEClassifiers().eachWithIndex{ it, i -> %>
    ${i>0?"++":""}model.n${it.name}List
<% } %>

getNextId: List ArcDef -> Int
getNextId arcs =
  let
    currentMax = List.maximum
                 <| List.map (\\x -> x.id) arcs
  in
    case currentMax of
      Just n ->
        n+1
      _ ->
        1
