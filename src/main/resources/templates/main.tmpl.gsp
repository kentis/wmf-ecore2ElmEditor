module Main exposing (..)

import App exposing (..)
import Update exposing(subscriptions, update)
import Browser exposing (element)
import Update exposing (..)
import Types exposing (..)



-- main : Program Model Msg
main =
    element { view = view, init = init, update = Update.update, subscriptions = subscriptions }
